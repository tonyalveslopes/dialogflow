package com.scio.module.dialogflow;

public class ResponseDTO {
	private String intent;
	private String uniqueId;

	public ResponseDTO(String intent, String uniqueId) {
		this.intent = intent;
		this.uniqueId = uniqueId;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	public String getIntent() {
		return intent;
	}

	public void setIntent(String intent) {
		this.intent = intent;
	}
	
	
}
