package com.scio.module.dialogflow;

import java.io.IOException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.gax.rpc.ApiException;

@RestController
@RequestMapping("/api/dialogflow/intent/detect")
public class IntentController {

	@PostMapping
	public ResponseDTO detect(@RequestBody Query query) {
		try {
			String intent = DetectIntentTexts.detectIntentTexts(query.getProjectId(),
					query.getText(), query.getSessionId(), query.getLanguageCode());
			return new ResponseDTO(intent, query.getSessionId());
		} catch (ApiException | IOException e) {
			return null;
		}
	}
}
