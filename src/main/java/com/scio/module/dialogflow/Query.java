package com.scio.module.dialogflow;

public class Query {
	private String projectId;
	private String text;
	private String sessionId;
	private String languageCode;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	@Override
	public String toString() {
		return "Query [projectId=" + projectId + ", text=" + text + "]";
	}
	
	
}
