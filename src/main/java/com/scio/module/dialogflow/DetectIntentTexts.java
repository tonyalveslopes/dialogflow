package com.scio.module.dialogflow;

import java.io.IOException;

import com.google.api.gax.rpc.ApiException;
import com.google.cloud.dialogflow.v2.DetectIntentResponse;
import com.google.cloud.dialogflow.v2.QueryInput;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.SessionName;
import com.google.cloud.dialogflow.v2.SessionsClient;
import com.google.cloud.dialogflow.v2.TextInput;

public class DetectIntentTexts {

	public static String detectIntentTexts(String projectId, String text, String sessionId, String languageCode)
			throws IOException, ApiException {
		String intentResult = null;
		try (SessionsClient sessionsClient = SessionsClient.create()) {
			SessionName session = SessionName.of(projectId, sessionId);
			TextInput.Builder textInput = TextInput.newBuilder().setText(text).setLanguageCode(languageCode);
			QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();
			DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);
			QueryResult queryResult = response.getQueryResult();
			intentResult = queryResult.getIntent().getDisplayName();
			System.out.println(intentResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return intentResult;
	}
}